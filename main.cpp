#include <iostream>
#include <fstream>

using namespace std;

inline unsigned char uncode ( char );

int main(int argc, char** argv)
{
    ifstream filein;
    fstream fileout;
    char inputFname[256], outputFname[256];
    cout << "input Filename" << endl;
    cin.get( inputFname, 256 ); cin.ignore();
    cout << "output Filename" << endl;
    cin.get( outputFname, 256 ); cin.ignore();
    filein.open (inputFname);
    //fileout.open (outputFname);
    if ( filein.is_open() )
    {
        cout << "input file " << inputFname << " is open successfully!"<< endl;
        fileout.open (outputFname);
        if ( fileout.is_open() )
        {
            cout << "output file " << outputFname << " is open successfully!"<< endl;
            unsigned char chr = 0;
            while ( filein.read ( ( char* )&chr, sizeof ( char ) ) )
            {
                chr = uncode ( chr );
                fileout.write ( ( char* )&chr, sizeof ( char ) );
            }
        }
        else
        {
            cout << "can't open output file " << outputFname << " !" << endl;
        }
    }
    else
    {
        cout << "can't open input file " << inputFname <<" !" << endl;
    }

    return 0;
}

inline unsigned char uncode ( char chr )
{
    return (~chr);
}